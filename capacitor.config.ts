import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'tp5',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
